'use strict';


/**
 * Get contracts
 * 
 *
 * returns Contracts
 **/
exports.getContract = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [
      {
      "name": "Akademický úvazek",
      "is_academic": true,
      "is_active": true,
      "visible": true,
      "from": 1604936793,
      "to": 1636472793
      },
      {
        "name": "Technický úvazek",
        "is_academic": false,
        "is_active": true,
        "visible": true,
        "from": 1604936793,
        "to": 1636472793
      },

    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

