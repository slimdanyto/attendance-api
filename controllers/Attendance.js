'use strict';

var utils = require('../utils/writer.js');
var Attendance = require('../service/AttendanceService');

module.exports.setToAttendance = function setToAttendance (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  var body = req.swagger.params['body'].value;
  Attendance.setToAttendance(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
